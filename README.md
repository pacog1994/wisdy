**AWCchatbot**

AWCchatbot folder is a virtual enviroment for developing the AI chatbot. All packages that are meant for this application should be put here.

Initialize AWCchatbot python environment with the following command on the commandline:
`source AWCchatbot/Scripts/activate`

Start AWCchatbot environment
`source AWCchatbot/Scripts/activate`

End AWCchatbot environment
`deactivate`

Helpful commands:

`pip install <package-name>` install packages
`pip list` shows what packages are installed
`python` use python shell
`py <python-file>` execute python file commandline

Wisdy app relies on ngrok to expose local through tunneling service
`./ngrok http 3000` calls ngrok http method on specified port 3000

Wisdy is using dockerized mongodb container

1. Make sure to run `docker-compose up` in the cmdline then check with `docker ps -a`
2. To execute mongodb in the terminal run `docker exec -it wisdy-mongo bash`
3. In the bash run `mongo admin -u root -p root` to access mongodb

Tagging in nltk https://www.nltk.org/book/ch05.html
