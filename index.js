const { init: initPoll } = require("./commands/poll")
const { init: initWisdyDocs } = require("./commands/wisdydocs/wisdydocs")
const { App, LogLevel } = require("@slack/bolt")
const dotenv = require("dotenv")

// Initializes your app with your bot token and signing secret
dotenv.config()

//global vars
global.app = {}

app = new App({
  token: process.env.SLACK_BOT_TOKEN,
  signingSecret: process.env.SLACK_SIGNING_SECRET
  //logLevel: LogLevel.DEBUG
})

//event triggered when user goes to app home
app.event("app_home_opened", async ({ payload, context }) => {
  const userId = payload.user

  try {
    //call the views.publish method using the built-in WebClient
    await app.client.views.publish({
      token: process.env.SLACK_BOT_TOKEN,
      user_id: userId,
      view: {
        type: "home",
        blocks: [
          {
            type: "section",
            text: {
              type: "mrkdwn",
              text: `What's up friend! <@${userId}> :`
            }
          },
          {
            type: "section",
            text: {
              type: "mrkdwn",
              text:
                "Thank you https://api.slack.com/method/views.publish/code for the help"
            }
          }
        ]
      }
    })
  } catch (error) {
    console.error(error)
  }
})

//initalize poll command listeners
initPoll()
//initialize wisdydocs command listeners
initWisdyDocs()

// Listens to incoming messages that contain "hello"
app.message("hello", async ({ message, say }) => {
  // say() sends a message to the channel where the event was triggered
  await say(`Suck a duh noodle <@${message.user}>! ${JSON.stringify(message)}`)
})

app.message(/^(feature|bug|hotfix)[0-9]+/, async ({ context, say }) => {
  //RegExp matches are inside of context.matches
  const processed = context.matches[0]
  await say(`adding ${processed}`)
})
;(async () => {
  // Start your app
  await app.start(process.env.PORT || 3000)

  console.log("⚡️ Bolt app is running!")
})()
