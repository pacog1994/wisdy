const { getUsers, putUser } = require("./wisdyDocsDAO")

const listAction = () => {
  app.view("view_list_action", async ({ ack, context, view }) => {
    const [channelId, userId] = view.private_metadata.split("-")
    await ack()
    try {
      let blocks = await getUsers()
      await app.client.chat.postEphemeral({
        token: context.botToken,
        channel: channelId,
        user: userId,
        text: "default ephemeral message - only you can see this",
        blocks
      })
    } catch (error) {
      console.error(error)
    }
  })
}

const addAction = () => {
  app.view("view_add_action", async ({ ack, say, request, params }) => {
    let requestBody = {
      name: "paco",
      age: 26,
      skills: ["coding", "running", "singing", "gaming"]
    }

    await ack()
    putUser(requestBody)
  })
}

module.exports = { listAction, addAction }
