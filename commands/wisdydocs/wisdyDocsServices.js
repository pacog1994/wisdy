const { getUsers, putUser, deleteUser } = require("./wisdyDocsDAO")

/**
 * Service that gets user data and processes to be displayed in Slack
 * @param {array} blocks Block array to be displayed
 */
const listUserService = async (blocks = []) => {
  const users = await getUsers()

  if (users.length > 0) {
    blocks.push(
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: `I found *${users.length}* users, take a look!`
        }
      },
      {
        type: "divider"
      }
    )
  } else {
    blocks.push({
      type: section,
      text: { type: "plain_text", text: "I found no users" }
    })
  }

  users.map((user) => {
    let block = {
      type: "section",
      text: {
        type: "mrkdwn",
        text: `${user.name}\nage: ${user.age}\nskills: ${
          Array.isArray(user.skills)
            ? user.skills.map((skill) => {
                return `${skill} `
              })
            : `${user.skills}`
        } `
      }
    }
    blocks.push(block, { type: "divider" })
  })
  return blocks
}

/**
 * Service to add user data into db and processes response sent to Slack
 * @param {object} req JSON request object to be sent to backend
 * @param {array} blocks Block array to be displayed
 */
const addUserService = async (req, blocks = []) => {
  const isUpserted = await putUser(req)

  isUpserted
    ? blocks.push({
        type: "section",
        text: {
          type: "mrkdwn",
          text: `*${req.name}* has been added to WisdyDocs!`
        }
      })
    : blocks.push({
        type: "section",
        text: {
          type: "mrkdwn",
          text: `A record was found in WisdyDocs matching the name: *${req.name}*. 
          Updating record instead!`
        }
      })
  return blocks
}

/**
 * Service to update user data into db and processes response sent to Slack
 * @param {object} req JSON request object to be sent to backend
 * @param {array} blocks Block array to be displayed
 */
const updateUserService = async (req, blocks = []) => {
  const isUpserted = await putUser(req)

  isUpserted
    ? blocks.push({
        type: "section",
        text: {
          type: "mrkdwn",
          text: `No record was found for *${req.name}*.\n
          A new record has been added to WisdyDocs!`
        }
      })
    : blocks.push({
        type: "section",
        text: {
          type: "mrkdwn",
          text: `*${req.name}'s* record has been updated in WisdyDocs!`
        }
      })

  return blocks
}

/**
 * Service to remove user in db and process response sent to Slack
 * @param {string} uid unique identifier used to search for user
 * @param {array} blocks Block array to be displayed
 */
const removeUserService = async (uid, blocks = []) => {
  const isDeleted = await deleteUser(uid)

  isDeleted
    ? blocks.push({
        type: "section",
        text: {
          type: "mrkdwn",
          text: `*${uid}* was found and deleted from WisdyDocs.`
        }
      })
    : blocks.push({
        type: "section",
        text: {
          type: "mrkdwn",
          text: `*${uid}* was not found. No record was removed from WisdyDocs.`
        }
      })

  return blocks
}

module.exports = {
  listUserService,
  addUserService,
  updateUserService,
  removeUserService
}
