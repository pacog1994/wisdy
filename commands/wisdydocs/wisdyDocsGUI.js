const selectAction = () => {
  app.action(
    "select_action",
    async ({ ack, action, body: { trigger_id, channel, user }, context }) => {
      await ack()
      try {
        switch (action.selected_option.value) {
          case "open_list_modal":
            openListModal(trigger_id, context, channel, user)
            break
          case "open_add_modal":
            openAddModal(trigger_id, context)
            break
          case "open_edit_modal":
            openEditModal(trigger_id, context)
            break
          case "open_remove_modal":
            openRemoveModal(trigger_id, context)
            break
        }
      } catch (error) {
        console.error(error)
      }
    }
  )
}

const openListModal = (trigger, context, channel, user) => {
  try {
    app.client.views.open({
      token: context.botToken,
      trigger_id: trigger,
      view: {
        type: "modal",
        callback_id: "view_list_action",
        title: {
          type: "plain_text",
          text: "List action",
          emoji: true
        },
        submit: {
          type: "plain_text",
          text: "Submit",
          emoji: true
        },
        close: {
          type: "plain_text",
          text: "Cancel",
          emoji: true
        },
        blocks: [
          {
            type: "input",
            element: {
              type: "plain_text_input"
            },
            label: {
              type: "plain_text",
              text: "Search Filter",
              emoji: true
            }
          }
        ],
        private_metadata: `${channel.id}-${user.id}`
      }
    })
  } catch (error) {
    console.error(error)
  }
}

const openAddModal = (trigger, context) => {
  try {
    app.client.views.open({
      token: context.botToken,
      trigger_id: trigger,
      view: {
        type: "modal",
        callback_id: "view_add_action",
        title: {
          type: "plain_text",
          text: "Add action",
          emoji: true
        },
        submit: {
          type: "plain_text",
          text: "Submit",
          emoji: true
        },
        close: {
          type: "plain_text",
          text: "Cancel",
          emoji: true
        },
        blocks: [
          {
            type: "section",
            text: {
              type: "plain_text",
              text: "Choose an element to insert"
            },
            accessory: {
              action_id: "select_action",
              type: "static_select",
              placeholder: {
                type: "plain_text",
                text: "Select an element"
              },
              options: [
                {
                  text: {
                    type: "plain_text",
                    text: "Skills"
                  },
                  value: "skills"
                },
                {
                  text: {
                    type: "plain_text",
                    text: "Project"
                  },
                  value: "project"
                },
                {
                  text: {
                    type: "plain_text",
                    text: "Website"
                  },
                  value: "website"
                }
              ]
            }
          },
          {
            type: "section",
            text: {
              type: "plain_text",
              text: "Choose an element to insert"
            }
          }
        ]
      }
    })
  } catch (error) {
    console.error(error)
  }
}

const openEditModal = (trigger, context) => {
  try {
    app.client.views.open({
      token: context.botToken,
      trigger_id: trigger,
      view: {
        type: "modal",
        callback_id: "view_edit_action",
        title: {
          type: "plain_text",
          text: "Edit action",
          emoji: true
        },
        submit: {
          type: "plain_text",
          text: "Submit",
          emoji: true
        },
        close: {
          type: "plain_text",
          text: "Cancel",
          emoji: true
        },
        blocks: [
          {
            type: "section",
            text: {
              type: "plain_text",
              text: "Editing"
            }
          }
        ]
      }
    })
  } catch (error) {
    console.error(error)
  }
}

const openRemoveModal = (trigger, context) => {
  try {
    app.client.views.open({
      token: context.botToken,
      trigger_id: trigger,
      view: {
        type: "modal",
        callback_id: "view_remove_action",
        title: {
          type: "plain_text",
          text: "Remove action",
          emoji: true
        },
        submit: {
          type: "plain_text",
          text: "Submit",
          emoji: true
        },
        close: {
          type: "plain_text",
          text: "Cancel",
          emoji: true
        },
        blocks: [
          {
            type: "section",
            text: {
              type: "plain_text",
              text: "Removing"
            }
          }
        ]
      }
    })
  } catch (error) {
    console.error(error)
  }
}

module.exports = { selectAction }
