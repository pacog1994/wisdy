const {
  listUserService,
  addUserService,
  updateUserService,
  removeUserService
} = require("./wisdyDocsServices")

const { selectAction } = require("./wisdyDocsGUI")

/**
 * Setup listeners for all poll events
 */
const init = function () {
  wisdydocs()
  selectAction()
}

const wisdydocs = () => {
  app.command("/wisdydocs", async ({ ack, command, context, payload }) => {
    const args = command.text.split(" ")
    await ack()

    switch (args[0]) {
      case "list":
        try {
          let blocks = await listUserService()
          await app.client.chat.postEphemeral({
            token: context.botToken,
            channel: payload.channel_id,
            user: payload.user_id,
            text: "user list",
            blocks
          })
        } catch (error) {
          console.error(error)
        } finally {
          break
        }
      case "add":
        try {
          const userData = await app.client.users.info({
            token: context.botToken,
            user: payload.user_id
          })
          let request = { name: userData.user.profile.display_name }
          //array of only request parameters from args
          const reqParams = args.slice(1)
          //build request from requet parameters
          request = buildRequestFromParams(request, reqParams)

          let blocks = await addUserService(request)

          await app.client.chat.postEphemeral({
            token: context.botToken,
            channel: payload.channel_id,
            user: payload.user_id,
            text: "user list",
            blocks
          })
        } catch (error) {
          console.error(error)
        } finally {
          break
        }
      case "update":
        try {
          const userData = await app.client.users.info({
            token: context.botToken,
            user: payload.user_id
          })
          let request = { name: userData.user.profile.display_name }
          //array of only request parameters from args
          const reqParams = args.slice(1)
          //build request from requet parameters
          request = buildRequestFromParams(request, reqParams)

          let blocks = await updateUserService(request)

          await app.client.chat.postEphemeral({
            token: context.botToken,
            channel: payload.channel_id,
            user: payload.user_id,
            text: "user list",
            blocks
          })
        } catch (error) {
          console.error(error)
        } finally {
          break
        }
      case "remove":
        try {
          // const userData = await app.client.users.info({
          //   token: context.botToken,
          //   user: payload.user_id
          // })
          let blocks = await removeUserService(args[1])
          await app.client.chat.postEphemeral({
            token: context.botToken,
            channel: payload.channel_id,
            user: payload.user_id,
            text: "removing user",
            blocks
          })
        } catch (error) {
          console.error(error)
        } finally {
          break
        }
      default:
        try {
          let blocks = [
            {
              type: "section",
              block_id: "wisdydocs1",
              text: {
                type: "mrkdwn",
                text: ":question: WisdyDocs Help :question:"
              }
            },
            {
              type: "section",
              text: {
                type: "plain_text",
                text: "Usage: wisdydocs COMMAND [PARAMETERS]",
                emoji: true
              }
            },
            {
              type: "section",
              text: {
                type: "plain_text",
                text: "A collection of user's public technical portfolio",
                emoji: true
              }
            },
            {
              type: "section",
              text: {
                type: "plain_text",
                text: "Commands:",
                emoji: true
              }
            },
            {
              type: "section",
              text: {
                type: "mrkdwn",
                text:
                  "*list <search-filter>* - Retrieves a list of users based off an *OPTIONAL* search filter\n\n*add <key=[age|skills]:value>* - Inserts user info by key and value paired by a : delimiter. Only age and skills are currently supported\n\n*update <key=[age|skills]:value>* - Updates user info by searching a key/value with a : delimiter. Only age and skills are curently supported.\n\n*remove <user>* - Removes a user from the collection of users based off the filter. Needs sufficient priviledges"
              }
            }
          ]

          await app.client.chat.postEphemeral({
            token: context.botToken,
            channel: payload.channel_id,
            user: payload.user_id,
            text: "default ephemeral message - only you can see this",
            blocks
          })
        } catch (error) {
          console.error(error)
        } finally {
          break
        }

      //DISABLED
      // let blocks = [
      //   {
      //     type: "section",
      //     block_id: "wisdydocs1",
      //     text: {
      //       type: "mrkdwn",
      //       text: ":book: Welcome to the *WisdyDocs GUI*! :book:"
      //     }
      //   },
      //   {
      //     type: "section",
      //     block_id: "wisdydocs2",
      //     text: {
      //       type: "plain_text",
      //       text: "Please select one of the following actions"
      //     },
      //     accessory: {
      //       action_id: "select_action",
      //       type: "static_select",
      //       placeholder: {
      //         type: "plain_text",
      //         text: "Select an item"
      //       },
      //       options: [
      //         {
      //           text: {
      //             type: "plain_text",
      //             text: "List Users"
      //           },
      //           value: "open_list_modal"
      //         },
      //         {
      //           text: {
      //             type: "plain_text",
      //             text: "Add User Data"
      //           },
      //           value: "open_add_modal"
      //         },
      //         {
      //           text: {
      //             type: "plain_text",
      //             text: "Edit User Data"
      //           },
      //           value: "open_edit_modal"
      //         },
      //         {
      //           text: {
      //             type: "plain_text",
      //             text: "Remove User Data"
      //           },
      //           value: "open_remove_modal"
      //         }
      //       ],
      //       confirm: {
      //         title: {
      //           type: "plain_text",
      //           text: "Are you sure?"
      //         },
      //         confirm: {
      //           type: "plain_text",
      //           text: "Yes"
      //         },
      //         deny: {
      //           type: "plain_text",
      //           text: "No, I've changed my mind!"
      //         }
      //       }
      //     }
      //   }
      // ]

      // try {
      //   await app.client.chat.postEphemeral({
      //     token: context.botToken,
      //     channel: payload.channel_id,
      //     user: payload.user_id,
      //     text: "default ephemeral message - only you can see this",
      //     blocks
      //   })
      // } catch (error) {
      //   console.error(error)
      // } finally {
      //   break
      // }
    }
  })
}

//Utility Methods
const buildRequestFromParams = (request = {}, reqParams) => {
  reqParams.forEach((reqParam) => {
    const kV = reqParam.split(":")
    const isArray = kV[1].split(",")
    if (isArray.length <= 1) {
      Object.assign(request, {
        [kV[0]]: kV[1]
      })
    } else {
      Object.assign(request, {
        [kV[0]]: isArray
      })
    }
  })

  return request
}

module.exports = { init }
