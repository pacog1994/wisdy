const axios = require("axios")

/**
 * Makes an http call to backend and returns a Promise of all user data
 */
const getUsers = async (filters = {}) => {
  return (users = await axios
    .get("http://localhost:4000/api/list")
    .then((res) => res.data))
}

/**
 * Makes an http call to backend and returns an upserted flag if the user was
 * inserted or updated
 * @param {object} req request JSON object
 */
const putUser = async (req) => {
  return (user = await axios
    .put("http://localhost:4000/api/update", req)
    .then((res) => res.data))
}

/**
 * Makes an http call to backend and returns a flag if the user was removed or not
 * @param {string} uid
 */
const deleteUser = async (uid) => {
  return (user = await axios
    .delete(`http://localhost:4000/api/remove/${uid}`)
    .then((res) => res.data))
}

module.exports = { getUsers, putUser, deleteUser }
