/**
 * Setup listeners for all poll events
 */
const init = function () {
  poll()
  vote()
  addOption()
  handleOptionSubmission()
}

/**
 * listens for poll command
 */
const poll = function () {
  app.command("/poll", async ({ command, ack, say }) => {
    //split comma-separated text into args
    const args = command.text.split(",")

    if (args.length > 1) {
      //acknowledge command
      await ack()

      let blocks = [
        {
          type: "section",
          text: {
            type: "mrkdwn",
            text: `${args[0]} - Poll by <@${command.user_id}>`
          }
        },
        {
          type: "divider"
        }
      ]

      // skip arg[0] -> prompt
      for (i = 1; i < args.length; i++) {
        blocks.push(
          {
            type: "section",
            text: {
              type: "mrkdwn",
              text: args[i]
            },
            accessory: {
              type: "button",
              text: {
                type: "plain_text",
                emoji: true,
                text: "Vote"
              },
              style: "primary",
              value: args[i],
              action_id: "btn_vote"
            }
          },
          {
            type: "context",
            elements: [
              {
                type: "plain_text",
                emoji: true,
                text: "0 votes"
              }
            ]
          }
        )
      }

      blocks.push(
        {
          type: "divider"
        },
        {
          type: "actions",
          elements: [
            {
              type: "button",
              text: {
                type: "plain_text",
                emoji: true,
                text: "Add an option"
              },
              action_id: "btn_add_option"
            }
          ]
        }
      )

      await say({ blocks })
    } else {
      await ack({
        errors: [
          {
            name: "missing params",
            error: "poll command requires 2+ params"
          }
        ]
      })
    }
  })
}

/**
 * listens for button vote action
 */
const vote = function () {
  app.action(
    "btn_vote",
    async ({ action, body: { user, channel, message }, context, ack }) => {
      //get user profile information
      const userData = await app.client.users.info({
        token: context.botToken,
        user: user.id
      })

      //required slack tools for adding
      let blockPairs = []
      let newBlocks = message.blocks
      //get each pair of section/context
      newBlocks.forEach((block, index) => {
        if (block.type === "section" && index !== 0) {
          blockPairs.push({ section: block, ctxt: newBlocks[index + 1], index })
        }
      })

      //index of block where user intends to vote; -1 = user doesn't intend to vote
      let addIndex = -1
      //flag to see if user intends to unvote
      let unvote = false

      blockPairs.forEach(({ section, ctxt, index }) => {
        let elements = ctxt.elements
        elements = cleanAttributes(elements)

        //if any reference to user is found delete it
        for (i = 0; elements.length - 1 >= i; i++) {
          if (elements[i].alt_text === userData.user.profile.display_name) {
            newBlocks = removeElement(newBlocks, index, i)
          }
        }

        //compare section to action block_id; true: assign index to addIndex; false: continue
        if (section.block_id === action.block_id) {
          addIndex = index
          for (i = 0; elements.length - 1 >= i; i++) {
            if (elements[i].alt_text === userData.user.profile.display_name) {
              unvote = true
              break
            }
          }
        }
      })
      //If addIndex is true, addElement. If addIndex is false, continue
      if (addIndex !== -1 && unvote === false) {
        newBlocks = addElement(newBlocks, addIndex, userData)
      }

      await ack()

      await app.client.chat.update({
        token: process.env.SLACK_BOT_TOKEN,
        channel: channel.id,
        ts: message.ts,
        as_user: true,
        blocks: newBlocks
      })
    }
  )
}

/**
 * listens for add suggestion action
 */
const addOption = function () {
  app.action(
    "btn_add_option",
    async ({ body, body: { channel, message }, ack, context }) => {
      // Acknowledge the action
      await ack()

      //TODO: open modal
      try {
        //Calls view.open to open modal
        await app.client.views.open({
          token: context.botToken,
          trigger_id: body.trigger_id,
          view: {
            type: "modal",
            //view identifier
            callback_id: "view_add_option",
            title: {
              type: "plain_text",
              text: "Add an Option",
              emoji: true
            },
            submit: {
              type: "plain_text",
              text: "Submit",
              emoji: true
            },
            close: {
              type: "plain_text",
              text: "Cancel",
              emoji: true
            },
            blocks: [
              {
                type: "input",
                element: {
                  type: "plain_text_input"
                },
                label: {
                  type: "plain_text",
                  text: "Option text",
                  emoji: true
                }
              }
            ],
            private_metadata: `${channel.id}-${message.ts}`
          }
        })
      } catch (error) {
        console.error(error)
      }
    }
  )
}

const handleOptionSubmission = function () {
  app.view("view_add_option", async ({ ack, view, context }) => {
    //destructure metadata
    const [channelId, messageTs] = view.private_metadata.split("-")
    //find conversation history specified by timestamp and channel
    let conversation = await app.client.conversations.history({
      token: context.botToken,
      channel: channelId,
      oldest: messageTs,
      latest: messageTs,
      inclusive: true
    })

    //first conversation index should be the message we want
    let newBlocks = conversation.messages[0].blocks
    const inputBlockId = view.blocks[0].block_id
    const plainTextInputBlockId = view.blocks[0].element.action_id
    const input = view.state.values[inputBlockId][plainTextInputBlockId].value

    //Clean all image blocks
    let blockArr = []
    newBlocks.forEach((block) => {
      if (block.type === "context") {
        blockArr.push({ ctxt: block })
      }
    })
    blockArr.forEach(({ ctxt }) => {
      let elements = ctxt.elements
      elements = cleanAttributes(elements)
    })

    //add new option section w/ button
    newBlocks.splice(newBlocks.length - 2, 0, {
      type: "section",
      text: {
        type: "mrkdwn",
        text: input
      },
      accessory: {
        type: "button",
        text: {
          type: "plain_text",
          emoji: true,
          text: "Vote"
        },
        style: "primary",
        value: input,
        action_id: "btn_vote"
      }
    })

    //add new context
    newBlocks.splice(newBlocks.length - 2, 0, {
      type: "context",
      elements: [
        {
          type: "plain_text",
          emoji: true,
          text: "0 votes"
        }
      ]
    })

    await ack()

    try {
      await app.client.chat.update({
        token: context.botToken,
        channel: channelId,
        ts: messageTs,
        as_user: true,
        blocks: newBlocks
      })
    } catch (error) {
      console.error(error)
    }
  })
}

//Utility Functions

/**
 * Add element to blocks
 * @param {object} blocks array of block elements to add to
 * @param {number} i index of associated context to insert into
 * @param {object} required array of necessary slack tools
 */
const addElement = function (blocks, i, userData) {
  //first half of original block as shallow copy
  const blockPt1 = blocks.slice(0, i + 1)
  //second half of original block as shallow copy
  const blockPt3 = blocks.slice(i + 2, blocks.length)
  //context to be changed
  let blockPt2 = blocks.slice(i + 1, i + 2)
  let elements = blockPt2[0].elements
  let image_url =
    "https://pbs.twimg.com/profile_images/625633822235693056/lNGUneLX_400x400.jpg"
  if (userData.user.profile.image_original) {
    image_url = userData.user.profile.image_original
  }

  try {
    //insert vote ctxt and store number of votes
    const voteCounter = elements.unshift({
      type: "image",
      image_url: image_url,
      alt_text: `${userData.user.profile.display_name}`
    })
    //update vote counter
    elements[elements.length - 1].text = `${voteCounter - 1} votes`
    //combine all parts together
    return blockPt1.concat(blockPt2, blockPt3)
  } catch (error) {
    console.log(error)
  }
}

// NEEDS TO BE OPTIMIZED
/**
 * Non-mutating removal of element at given index
 * @param {object} blocks array of block elements to add to
 * @param {number} i index of associated context
 * @param {number} rIndex index of element to be removed
 * @param {object} required array of necessary slack tools
 */
const removeElement = function (blocks, i, rIndex) {
  const blockPt1 = blocks.slice(0, i + 1)
  const blockPt3 = blocks.slice(i + 2, blocks.length)
  let blockPt2 = blocks.slice(i + 1, i + 2)
  const ctxtPt1 = blockPt2[0].elements.slice(0, rIndex)
  const ctxtPt2 = blockPt2[0].elements.slice(
    rIndex + 1,
    blockPt2[0].elements.length
  )
  blockPt2[0].elements = ctxtPt1.concat(ctxtPt2)
  const elements = blockPt2[0].elements

  elements[elements.length - 1].text = `${elements.length - 1} votes`
  return (newBlocks = blockPt1.concat(blockPt2, blockPt3))
}

/**
 * Clean Slack add-on attributes for images
 * @param {object} elements elements to cleanse of Slacks notorious add-ons
 */
const cleanAttributes = function (elements) {
  //for every element --> look into and remove fallback, image_width, image_height, image_bytes
  elements.forEach((element) => {
    if (element.fallback) {
      delete element["fallback"]
    }
    if (element.image_width) {
      delete element["image_width"]
    }
    if (element.image_height) {
      delete element["image_height"]
    }
    if (element.image_bytes) {
      delete element["image_bytes"]
    }
    if (element.is_animated !== undefined) {
      delete element["is_animated"]
    }
  })

  return elements
}

module.exports = { init }
